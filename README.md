# musical typing

this is a html page that triggers sample playback from key strokes.

the 16 samples, created by [Beatsbyjblack](https://soundcloud.com/beatsbyjblack) and provided by [novation](https://novationmusic.com/) for their [finger drumming contest](https://novationmusic.com/en/finger-drumming-contest-2020), are located in the sample directory and are included in the page as audio tags. they are named sample01 - sample16 and are ordered starting with kick, snare, hi hats, ride and crash, followed by melodic, vocal and miscellaneous samples.

most keys on the keyboard are mapped to a sample and captured key strokes result in playback of the sample. the mapping is done on english language letter frequency basis, with the two most frequent letter assigned to sample01, up to sample10, and the last 6 letters assigned to sample11 - sample16.

the number of samples that can be used is arbitrary and only limited to the number of keys on the keyboard used.

## improvements
### automatic sample to key assignment

read the samples from the directory and use a character frequency map to assign samples to keys. this would require the samples to be named so that the samples that should be played more frequently are assigned to keys that are used more frequently when typing. character frequency varies between languages so a language selector could be useful although most languages other than English use more characters and diacritical signs.

## links

https://keycode.info/
https://en.wikipedia.org/wiki/Letter_frequency